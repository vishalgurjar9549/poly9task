
const mongoose = require("mongoose");

const NoteSchema = new mongoose.Schema({
    noteName:{
        type:String,
        required:true
    },
    techPartCode:{
        type:Number,
        required:true
    },
});

const Note = mongoose.model("Note",NoteSchema)

module.exports = Note;