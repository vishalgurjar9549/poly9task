const express = require("express")
const mongoose = require("mongoose")

const app = express();

const NotesModle = require("./models/Note");

app.use(express.json());

mongoose.connect(
    "mongodb+srv://vishal01:vishalalpha@cluster0.kybpp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    {
    useNewUrlParser:true,
});

app.get("/",async (req,res)=> {
const note = new NotesModle({noteName:"javaScript",techPartCode:2});

try {
    await note.save();
}catch (err) {
    console.log(err)
}
})

app.listen(3001,()=>{
    console.log("server running on port 3001....")
})